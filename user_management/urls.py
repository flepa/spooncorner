from django.urls import path
from django.contrib.auth import views as auth_views
from user_management import views

app_name = 'user_management'

urlpatterns = [
    path('profile/create', views.ProfileCreateView.as_view(), name='profile-create'),
    path('profile', views.ProfileDetailView.as_view(), name='profile'),
    path('profile/update', views.ProfileUpdateView.as_view(), name='profile-update'),

    path('user/register', views.UserCreateView.as_view(), name='user-register'),
    path('user/login', views.UserLoginView.as_view(), name='login'),
    path('user/logout', auth_views.LogoutView.as_view(), name='logout'),
    path('user/update_email', views.UserUpdateEmailView.as_view(), name='user-update-email'),
    path('user/delete', views.UserDeleteView.as_view(), name='user-delete'),
    path('check_username_unique', views.check_username_is_unique, name='check-username-unique'),
    path('check_email_unique', views.check_email_is_unique, name='check-email-is-unique'),
    path('verify/<str:user_id_b64>/<str:user_token>', views.verify_user_email, name='verify-user-email'),
]
