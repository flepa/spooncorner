from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field, HTML, Row, Column
from django import forms
from django.contrib.auth.forms import UserCreationForm, SetPasswordForm, AuthenticationForm
from user_management.models import Profile, PlatformUser
from django.utils.translation import gettext_lazy as _


class PlatformUserCreationForm(UserCreationForm):
    """
    Form used to create a platform user.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Default form settings
        self.fields['username'].widget.attrs['placeholder'] = _('Nome utente')
        self.fields['email'].required = True
        self.fields['email'].widget.attrs['placeholder'] = _('username@dominio.com')
        self.fields['password1'].widget.attrs['placeholder'] = _('Scegli la tua password con saggezza')
        self.fields['password2'].widget.attrs['placeholder'] = _('Ripeti la password scelta')
        for field_name in ['username', 'password1', 'password2']:
            self.fields[field_name].help_text = None
        self.fields['terms_of_service_acceptance'].required = True
        # Crispy settings
        self.helper = FormHelper()
        self.helper.form_id = 'user-register-crispy-form'
        self.helper.form_class = ''
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            HTML('<h1 class="h3 mb-3 font-weight-bold text-center" style="color: white;">Registrati su Spoon '
                 'Corner</h1>'),
            Row(
                Column('username', css_class='form-group col-md-6 mb-0'),
                Column('email', css_class='form-group col-md-6 mb-0'),
            ),
            'password1',
            'password2',
            Row(Column('terms_of_service_acceptance', css_class='text-center')),
            Row(
                Column(
                    Submit('submit', _('Registrati'), css_class='btn btn-lg btn-secondary')
                ), css_class='text-center'),
        )

    class Meta:
        model = PlatformUser
        fields = (
            'username',
            'email',
            'password1',
            'password2',
            'terms_of_service_acceptance',
        )
        labels = {
            'terms_of_service_acceptance': 'Accettazione dei termini di servizio'
        }


class LoginForm(AuthenticationForm):
    """
    Form used to login a user.
    """
    error_messages = {
        'invalid_login': "Username o password errati. Perfavore, controlla i tuoi dati.",
        'inactive': "Questo account non è più attivo.",
    }

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        self.user_cache = None

        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = 'user-login-crispy-form'
        self.helper.form_class = 'form-signin text-center'
        self.helper.form_method = 'POST'
        self.fields['username'].label = ''
        self.fields['password'].label = ''
        self.helper.layout = Layout(
            HTML('<h1 class="h3 mb-3 font-weight-bold" style="color: white;">Accedi</h1>'),
            Field('username', css_class='form-group', id='inputEmail', type='email', placeholder='Email'),
            Field('password', css_class='form-group', id='inputPassword', placeholder='Password'),
            Submit('submit', _('Continua'), css_class='btn btn-lg btn-secondary btn-block'),
            HTML(
                '''
                    <br>
                    <div class="row d-flex flex-column">
                        <div class="column p-2">
                            <a href="{% url "password_reset" %}" class="btn btn-secondary">Ripristina password <i class="fas fa-key"></i></a>
                        </div>
                        <div class="column p-2">
                            <a href="{% url "user_management:user-register" %}" class="btn btn-secondary">
                            Registrati <i class="fas fa-home"></i></a>
                        </div>
                    </a>
                    </div>
                '''
            )
        )


class PlatformUserEmailOnlyForm(forms.ModelForm):
    """
    Form used to update only the email of a user.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(
            Submit('submit', _('Conferma'), css_class='btn btn-info')
        )
        if self.instance.id:
            self.helper.add_input(
                Submit('cancel', _('Annulla'), css_class='btn btn-danger', formnovalidate='formnovalidate')
            )
        self.helper.layout = Layout(
            Row(
                Column('email', css_class='form-group col-md-6 mb-0', required=True),
                css_class='grey-text'
            ),
        )

    class Meta:
        model = PlatformUser

        fields = (
            'email',
        )
        labels = {
            'email': 'Email'
        }


class PlatformUserSetNewPasswordForm(SetPasswordForm):
    """
    Form used to set a new password for a user.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name in ['new-password1', 'new-password2']:
            self.fields[field_name].help_text = None
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(
            Submit('submit', 'Invia', css_class='btn btn-lg btn-warning btn-block my-5')
        )
        self.helper.layout = Layout(
            Row(
                Column('new-password1', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('new-password2', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
        )


class SubmitButtonMixin(forms.ModelForm):
    """
    Form "footer" used to manage the two possible scenario,
    a user create (create button) or a user update (update button)
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.id:
            self.helper.inputs[0].value = _('Aggiorna')
            self.helper.inputs[0].field_classes = 'btn btn-info'
            self.helper.inputs[1].field_classes = 'btn btn-danger'
        else:
            self.helper.inputs[0].value = _('Crea')
            self.helper.inputs[0].field_classes = 'btn btn-lg btn-secondary'
            self.helper.inputs[1].field_classes = 'd-none'


class ProfileCrispyForm(SubmitButtonMixin):
    """
    Form used to create or update a user's profile.
    """
    helper = FormHelper()
    helper.form_id = 'person-crispy-form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', _('Save')))
    helper.add_input(Submit('cancel', _('Annulla'), formnovalidate='formnovalidate'))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['placeholder'] = _('Nome')
        self.fields['last_name'].widget.attrs['placeholder'] = _('Cognome')
        self.fields['street'].widget.attrs['placeholder'] = _('Indirizzo')
        self.fields['number'].widget.attrs['placeholder'] = _('Numero')
        self.fields['city'].widget.attrs['placeholder'] = _('Città')
        self.fields['province'].widget.attrs['placeholder'] = _('Provincia')
        self.fields['zip'].widget.attrs['placeholder'] = _('CAP')
        if self.instance.id:  # form in modifica
            self.helper.layout = Layout(
                Row(
                    Column('first_name', css_class='col-md-6 mb-0'),
                    Column('last_name', css_class='col-md-6 mb-0'),
                    css_class='form-row'
                ),
                Row(
                    Column('street', css_class='col-md-6 mb-0'),
                    Column('number', css_class='col-md-4 mb-0'),
                    Column('zip', css_class='col-md-2 mb-0'),
                    css_class='form-row'
                ),
                Row(
                    Column('city', css_class='col-md-6 mb-0'),
                    Column('province', css_class='col-md-2 mb-0'),
                    Column('zone', css_class='col-md-4 mb-0'),
                    css_class='form-row'
                ),
            )
        else:  # form in inserimento
            self.helper.layout = Layout(
                HTML('<h1 class="h3 mb-3 font-weight-bold text-center" style="color: white;">Crea il tuo Profilo</h1>'),
                Row(
                    Column('first_name', css_class='col-md-6 mb-0'),
                    Column('last_name', css_class='col-md-6 mb-0'),
                    css_class='form-row'
                ),
                Row(
                    Column('street', css_class='col-md-6 mb-0'),
                    Column('number', css_class='col-md-4 mb-0'),
                    Column('zip', css_class='col-md-2 mb-0'),
                    css_class='form-row'
                ),
                Row(
                    Column('city', css_class='col-md-6 mb-0'),
                    Column('province', css_class='col-md-2 mb-0'),
                    Column('zone', css_class='col-md-4 mb-0'),
                    css_class='form-row'
                ),
            )

    class Meta:
        model = Profile
        fields = (
            'first_name',
            'last_name',
            'street',
            'number',
            'city',
            'province',
            'zip',
            'zone'
        )
        labels = {
            'first_name': 'Nome',
            'last_name': 'Cognome',
            'street': 'Indirizzo',
            'city': 'Città',
            'province': 'Provincia',
            'number': 'Numero',
            'zip': 'CAP',
            'zone': 'Zona'
        }
