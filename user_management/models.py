from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from dishes.models import OrderHeader, Dish
from restaurant_project import settings


class PlatformUser(AbstractUser):
    """
    Model that represent a user of the application. The username field is the
    email field.
    """
    AbstractUser._meta.get_field('email')._unique = True
    terms_of_service_acceptance = models.BooleanField(default=False)
    terms_of_service_acceptance_datetime = models.DateTimeField(auto_now_add=True)
    is_manager = models.BooleanField(default=False)

    REQUIRED_FIELDS = []
    USERNAME_FIELD = 'email'

    def clean(self):
        """
        A user have to accept the term of services.
        :return: Exception.
        """
        if not self.terms_of_service_acceptance:
            raise ValidationError(_("You must accept the terms of services"))

    class Meta:
        verbose_name = _('platform user')
        verbose_name_plural = _('platform users')


class Profile(models.Model):
    """
    Model that represent a profile of a correctly registered user.
    """
    A_ZONE = 'A'
    B_ZONE = 'B'
    C_ZONE = 'C'
    D_ZONE = 'D'
    ZONES = [
        (A_ZONE, _('A - Centro storico di Modena')),
        (B_ZONE, _('B - Zona urbana di Modena')),
        (C_ZONE, _('C - Frazioni di Modena')),
        (D_ZONE, _('D - Provincia di Modena')),
    ]
    DELIVERY_TIME = {
        A_ZONE: 5,
        B_ZONE: 10,
        C_ZONE: 15,
        D_ZONE: 30
    }
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    street = models.CharField(max_length=120, null=True, blank=True)
    number = models.PositiveIntegerField(null=True, blank=True)
    city = models.CharField(max_length=120, null=True, blank=True)
    province = models.CharField(max_length=120, null=True, blank=True)
    zip = models.CharField(max_length=5, null=True, blank=True)
    zone = models.CharField(max_length=2, choices=ZONES, null=True, blank=True)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='profile'
    )

    class Meta:
        ordering = ['-last_name', 'first_name']

    def __str__(self):
        return f'Profilo di {self.first_name} {self.last_name}'

    def create_cart(self):
        """
        When a user is created or when he complete his current order, a new
        cart is created.
        """
        cart = OrderHeader.objects.create(user=self.user)
        cart.code = _(f'ORDINE # {cart.pk}')
        cart.save()

    @property
    def get_address(self):
        """
        If all the address components are correctly stored, returns them
        otherwise returns None
        :return: The user address or None.
        """
        if self.city and self.street and self.number and self.province and self.zip and self.zone:
            return f'{self.street} {self.number}, {self.city} ({self.province}) - {self.zip} [{self.zone}]'
        else:
            return None

    @property
    def get_cart(self):
        """
        Returns the current user cart.
        :return: User cart.
        """
        return OrderHeader.objects.get(user=self.user, state=OrderHeader.CART)

    @property
    def get_expected_delivery_time(self):
        """
        Retrieves the delivery time from the dictionary inside the model.
        :return: The expected delivery time.
        """
        return self.__class__.DELIVERY_TIME[self.zone]

    @property
    def get_orders_count(self):
        """
        Returns the sum of all the orders made by the current user.
        :return: The number of orders made by the user.
        """
        return OrderHeader.objects.filter(user=self.user).exclude(state=OrderHeader.CART).count()

    @property
    def get_all_ordered_dishes(self):
        """
        Returns all the ordered dishes grouped in categories.
        :return: A dictionary that have the categories as keys and lists of dishes as values.
        """
        orders = OrderHeader.objects.filter(user=self.user).exclude(state=OrderHeader.CART)
        categories = Dish.get_dish_categories()

        for order in orders:
            for item in order.get_items:
                categories[item.dish.category].extend([item.dish
                                                       for item in order.get_items
                                                       if item.dish not in categories[item.dish.category]])
        return categories
