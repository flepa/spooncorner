from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test

from restaurant_project import settings


def manager_only(view_func):
    """
    Decorator that returns a function which protects the access to sensitive
    views, only a manager can access this views.
    :param view_func: View to decorate.
    :return: Decorated function.
    """
    user_is_manager = user_passes_test(
        lambda user: user.is_manager,
        login_url=settings.LOGIN_URL
    )
    return login_required(user_is_manager(view_func))
