from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.auth.views import LoginView
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, Http404, HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic import CreateView, TemplateView, UpdateView
from django.utils.translation import gettext_lazy as _
from django.views.generic.base import View

from user_management.forms import PlatformUserCreationForm, ProfileCrispyForm, PlatformUserEmailOnlyForm, LoginForm
from user_management.models import PlatformUser, Profile

account_activation_token = PasswordResetTokenGenerator()


################ USER ################


class UserCreateView(CreateView):
    """
    View to create a new user.
    """
    form_class = PlatformUserCreationForm
    template_name = 'registration/user_crispy_create.html'
    success_url = reverse_lazy('homepage')

    def form_valid(self, form):
        """
        A user need to confirm his email before gaining access to the web
        application.
        :param form: PlatformUser form.
        :return: HTTP response.
        """
        response = super(UserCreateView, self).form_valid(form)

        mail_subject = _('Spoon Corner: Conferma account')
        relative_confirm_url = reverse(
            'user_management:verify-user-email',
            args=[
                urlsafe_base64_encode(force_bytes(self.object.pk)),
                account_activation_token.make_token(self.object)
            ]
        )
        self.object.email_user(
            subject=mail_subject,
            message=(f'Ciao {self.object.username}, \n\n'
                     f'Benvenuto sul nostro sito!\n'
                     f'Clicca il link seguente per confermare il tuo account: \n'
                     f'{self.request.build_absolute_uri(relative_confirm_url)}\n'
                     f'\n'
                     f'A presto,\n'
                     f'Il team di Spoon Corner'),
        )
        self.object.token_sent = True
        # until the user confirms his email, he is inactive
        self.object.is_active = False
        self.object.save()
        return response


class UserLoginView(LoginView):
    """
    View to display the login form.
    """
    form_class = LoginForm
    template_name = 'registration/login.html'
    success_url = reverse_lazy('homepage')


class UserUpdateEmailView(LoginRequiredMixin, UpdateView):
    """
    View to update the email of a user.
    """
    model = PlatformUser
    template_name = 'user_management/email_crispy_update.html'
    form_class = PlatformUserEmailOnlyForm
    success_url = reverse_lazy('user_management:profile')

    def get_object(self, queryset=None):
        """
        Returns the requested user.
        """
        return PlatformUser.objects.get(pk=self.request.user.pk)

    def post(self, request, *args, **kwargs):
        """
        In order to manage the cancel button from the dish form. If 'cancel'
        is in the request.POST, the dish must not be updated.
        :return: HTTP response.
        """
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse_lazy('user_management:profile'))
        else:
            return super(UserUpdateEmailView, self).post(request, *args, **kwargs)


class UserDeleteView(LoginRequiredMixin, View):
    """
    View to delete an existing platform user.
    """
    def get(self, request, *args, **kwargs):
        PlatformUser.objects.get(pk=self.request.user.pk).delete()
        return HttpResponseRedirect(reverse_lazy('user_management:login'))

    def post(self, request, *args, **kwargs):
        PlatformUser.objects.get(pk=self.request.user.pk).delete()
        return HttpResponseRedirect(reverse_lazy('user_management:login'))


################ PROFILE ################


class ProfileCreateView(LoginRequiredMixin, CreateView):
    """
    View to display
    """
    form_class = ProfileCrispyForm
    template_name = 'registration/profile_crispy_create.html'
    success_url = reverse_lazy('homepage')

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.create_cart()
        return super(ProfileCreateView, self).form_valid(form)


class ProfileDetailView(LoginRequiredMixin, TemplateView):
    """
    View to display the profile of the authenticated user.
    """
    template_name = 'user_management/user_profile.html'
    profile = None

    def get_context_data(self, **kwargs):
        context = super(ProfileDetailView, self).get_context_data(**kwargs)
        try:
            self.profile = get_object_or_404(Profile, user=self.request.user)
        except ObjectDoesNotExist:
            raise Http404

        return context


class ProfileUpdateView(UpdateView, LoginRequiredMixin):
    """
    View to update the information about the profile of an authenticated user.
    """
    model = Profile
    template_name = 'user_management/profile_crispy_update.html'
    form_class = ProfileCrispyForm
    success_url = reverse_lazy('user_management:profile')

    def get_object(self, queryset=None):
        """
            Returns the requested profile.
        """
        return Profile.objects.get(user=self.request.user)

    def post(self, request, *args, **kwargs):
        """
        In order to manage the cancel button from the dish form. If 'cancel'
        is in the request.POST, the dish must not be updated.
        :return: HTTP response.
        """
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse_lazy('user_management:profile'))
        else:
            return super(ProfileUpdateView, self).post(request, *args, **kwargs)


################ AJAX REQUESTS ################


def check_username_is_unique(request):
    """
    Function called by an ajax request, it checks if the given username is
    unique inside the platform.
    :return: The check results.
    """
    user = None
    username = request.GET.get('username', False)
    if username:
        user = PlatformUser.objects.filter(username=username)
    return JsonResponse({'is_not_unique': True if user else False})


def check_email_is_unique(request):
    """
    Function called by an ajax request, it checks if the given email is
    unique inside the platform.
    :return: The check results.
    """
    user = None
    email = request.GET.get('email', False)
    if email:
        user = PlatformUser.objects.filter(email=email)  # deve anche avere l'id diverso dallo user corrente!
    return JsonResponse({'is_not_unique': True if user else False})


def user_login_by_token(request, user_id_b64=None, user_token=None):
    """
    Function to check if the current user is correctly related to his user_id
    and his token.
    :param request: The current request.
    :param user_id_b64: The user_id in base 64.
    :param user_token: The user token.
    :return:
    """
    try:
        uid = force_text(urlsafe_base64_decode(user_id_b64))
        user = PlatformUser.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, PlatformUser.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, user_token):
        user.is_active = True
        user.save()
        login(request, user)
        return True

    return False


def verify_user_email(request, user_id_b64=None, user_token=None):
    """
    View to check if the current user is correctly related to his user_id
    and his token, if it's true, the user can create his platoform profile.
    :param request: The current request.
    :param user_id_b64: The user_id in base 64.
    :param user_token: The user token.
    :return:
    """
    if not user_login_by_token(request, user_id_b64, user_token):
        message = _('Errore: Tentativo di validazione email per l\'utente {user} con token {token}')
        subject = _('Errore di autenticazione')
        # in this case manager and admin are the same entity
        manager = PlatformUser.objects.get(is_manager=True)
        manager.email_user(subject=subject, message=message)
    return redirect('user_management:profile-create')


def login_user_for_password_reset(request, user_id_b64=None, user_token=None):
    """
    View to check if the current user is correctly related to his user_id
    and his token, if it's true, the user can reset his password.
    :param request: The current request.
    :param user_id_b64: The user_id in base 64.
    :param user_token: The user token.
    :return:
    """
    if not user_login_by_token(request, user_id_b64, user_token):
        message = 'Errore: Tentativo di reset password non autorizzato per l\'utente {user} con token {token}'
        subject = _('Errore di reset password')
        # in this case manager and admin are the same entity
        manager = PlatformUser.objects.get(is_manager=True)
        manager.email_user(subject=subject, message=message)

        return redirect('homepage')
    return redirect('user_background:set-new-password')
