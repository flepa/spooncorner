# Spoon Corner

The web application is available online [here](https://spooncorner.pythonanywhere.com/).

**Note**: keep in mind that this is only a university project about web applications, it's main focus is the Django backend development, sorry for the not optimal user experience.

## Installation Instructions

- #### Requirements
  
  The  packages (they are explained in details in the file **tesina_spooncorner_fontana.pdf**) required by Spoon Corner are listed in the `Pipfile` file, they can be installed with the following terminal command:
  
  ```bash
  pipenv install
  ```

- ### Setting your Django Secret Key
  
  Create a `/restaurant_project/secret_key.txt` file to store your secret key, this file will be read in `settings.py`

- ### *uploads/* and *MEDIA_URL* & *MEDIA_ROOT*
  
  The application works with images, for the correct display of images, is necessary to update `MEDIA_URL` and `MEDIA_ROOT` variables (in `settings.py`) with the deisired location, the given `uploads/` folder have to be moved to this location.

- ### Custom email settings
  
  The project works with a standard email address: *spooncorner.django@gmail.com*, its credentials are listed in the file `restaurant_project/custom_email_settings.py`, the address should work without any problem, but it's possible that Google blocks the access for requests from a different IP address, in this case contact me (*238290@studenti.unimore.it*) for allowing the access from "non secure apps". 
  
  For **testing** purposes, it's **recommended** to change this address with another preferred one or just include the template file (`email_settings.py`) in the `settings.py` file.

- ### Accessing the web application
  
  The web app can be accessed with a new account, but in case, the admin account credentials are:
  
  - **email**: spooncorner.django@gmail.com
  
  - **password**: mescoli123
  
  a test account have been created, his credentials are:
  
  - **email**: 238290@studenti.unimore.it
  
  - **password**: mescoli123
