from django.urls import path
from dishes import views

app_name = 'dishes'

urlpatterns = [
    path('cart', views.CartListView.as_view(), name='cart'),
    path('get_cart_items', views.get_cart_items, name='get-cart-items'),
    path('remove_from_cart', views.remove_from_cart, name='remove-from-cart'),
    path('add_to_cart', views.add_dish_to_cart, name='add-dish-to-cart'),
    path('order/<int:pk>/recap', views.OrderRecapView.as_view(), name='order-recap'),
    path('order/<int:pk>/complete', views.OrderCompleteView.as_view(), name='order-complete'),
    path('order/<int:pk>/detail', views.OrderDetailView.as_view(), name='order-detail'),
    path('order/list', views.OrderListView.as_view(), name='order-list'),

    path('review/add/<int:pk>', views.ReviewCreateView.as_view(), name='review-add'),
    path('review/<int:pk>/detail', views.ReviewDetailView.as_view(), name='review-detail'),
    path('review/<int:pk>/update', views.ReviewUpdateView.as_view(), name='review-update'),
    path('review/<int:pk>/delete', views.ReviewDeleteView.as_view(), name='review-delete'),

    path('menu', views.MenuView.as_view(), name='menu'),
    path('dish/add', views.DishAddView.as_view(), name='add-dish'),
    path('dish/<int:pk>/detail', views.DishDetailView.as_view(), name='dish-detail'),
    path('dish/<int:pk>/update', views.DishUpdateView.as_view(), name='dish-update'),
    path('dish/<int:pk>/delete', views.DishDeleteView.as_view(), name='dish-delete'),
    path('dish/search', views.search_dishes, name='dish-search'),
    path('dish/search/view', views.DishSearchView.as_view(), name='dish-search-view'),
]
