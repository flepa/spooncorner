from crispy_forms.bootstrap import AppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, HTML, Row, Column, Submit, Field
from django import forms
from django.utils.translation import gettext_lazy as _

from dishes.models import Dish, OrderHeader, Review


class DishForm(forms.ModelForm):
    """
    Form used to create or update a dish.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Default form settings
        self.fields['name'].widget.attrs['placeholder'] = _('Nome piatto')
        self.fields['ingredients'].widget.attrs['placeholder'] = _('Lista degli ingredienti...')
        self.fields['cook_time'].widget.attrs['placeholder'] = _('Tempo di preparazione')
        self.fields['price'].widget.attrs['placeholder'] = _('Prezzo del piatto')
        # Crispy settings
        self.helper = FormHelper()
        self.helper.form_id = 'dish-add-crispy-form'
        self.helper.form_class = ''
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='col-md-6 mb-0'),
                Column(Field('category', css_class='custom-select'), css_class='col-md-6 mb-0'),
                css_class='grey-text'
            ),
            Row(Column('image', css_class='mb-0'), css_class='grey-text'),
            Row(
                Column(AppendedText('cook_time', 'min', active=True), css_class='col-md-6 mb-0'),
                Column(AppendedText('price', '€', active=True), css_class='col-md-6 mb-0'),
                css_class='grey-text'
            ),
            Row(Column(Field('ingredients', type='textarea'), css_class='mb-0'), css_class='grey-text'),
            Submit('submit', _('Salva'), css_class='btn btn-info'),
            Submit('cancel', _('Annulla'), css_class='btn btn-danger', formnovalidate='formnovalidate'),
        )

    class Meta:
        model = Dish
        fields = (
            'name',
            'category',
            'ingredients',
            'cook_time',
            'price',
            'image',
        )
        labels = {
            'name': 'Nome del piatto',
            'category': 'Categoria',
            'ingredients': 'Ingredienti',
            'cook_time': 'Tempo di preparazione (min)',
            'price': 'Prezzo',
            'image': 'Foto del piatto',
        }


class OrderHeaderForm(forms.ModelForm):
    """
    Form to update an order state (if the user is a manager).
    """
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('initial', None).pop('user', None)
        super().__init__(*args, **kwargs)
        # Default form settings
        self.fields['code'].disabled = True
        self.fields['amount'].disabled = True
        if not user.is_manager:
            self.fields['state'].disabled = True
        self.fields['delivery_type'].disabled = True
        self.fields['date'].disabled = True
        self.fields['notes'].disabled = True
        # Crispy settings
        self.helper = FormHelper()
        self.helper.form_id = 'order-detail-crispy-form'
        self.helper.form_class = ''
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            Row(
                Column('code', css_class='col-md-6 mb-0'),
                Column(AppendedText('amount', '€', active=True), css_class='col-md-3 mb-0'),
                Column('delivery_type', css_class='col-md-3 mb-0 pl-5 mt-4 custom-control custom-switch'),
                css_class='d-flex align-items-center grey-text'
            ),
            Row(
                Column(Field('state', css_class='custom-select'), css_class='col-md-6 mb-0'),
                Column('date', css_class='col-md-6 mb-0'),
                css_class='grey-text'
            ),
            Row(
                Column(Field('notes', type='textarea', rows="3"), css_class='mb-0'), css_class='grey-text'
            )
        )
        if kwargs['instance'].delivery_type:
            self.helper.layout.append(
                HTML("""
                    <hr>
                    <h4 class="font-weight-light">
                        Tempo di consegna previsto: {{ orderheader|get_preparation_time:orderheader.user }} minuti 
                        <i class="far fa-clock"></i>
                    </h4>
                """)
            )
        else:
            self.helper.layout.append(
                HTML("""
                    <hr>
                    <h4 class="font-weight-light">
                        Tempo di preparazione previsto: {{ orderheader|get_preparation_time:orderheader.user }} minuti 
                        <i class="far fa-clock"></i>
                    </h4>
                """)
            )
        if user.is_manager:
            self.helper.add_input(Submit('submit', _('Aggiorna'), css_class='btn btn-info'))
            self.helper.add_input(Submit('cancel', _('Annulla'),
                                         css_class='btn btn-danger',
                                         formnovalidate='formnovalidate'))

    class Meta:
        model = OrderHeader
        fields = (
            'code',
            'amount',
            'state',
            'delivery_type',
            'date',
            'notes',
        )
        labels = {
            'code': 'Codice ordine',
            'amount': 'Totale',
            'state': 'Stato ordine',
            'delivery_type': 'Domicilio',
            'date': 'Data ordine',
            'notes': 'Note',
        }


class ReviewForm(forms.ModelForm):
    """
    Form used to create or update a review.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Default form settings
        self.fields['title'].widget.attrs['placeholder'] = _('Titolo')
        self.fields['comment'].widget.attrs['placeholder'] = _('Scrivi la tua esperienza...')
        # Crispy settings
        self.helper = FormHelper()
        self.helper.form_id = 'review-add-crispy-form'
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            Row(HTML(
                """
                    <label>Valutazione complessiva *</label>
                    <div class="form-group">
                        <fieldset class="rating">
                            <input type="radio" id="star5" name="stars" value="5" />
                            <label for="star5">5 stars</label>
                            <input type="radio" id="star4" name="stars" value="4" />
                            <label for="star4">4 stars</label>
                            <input type="radio" id="star3" name="stars" value="3" />
                            <label for="star3">3 stars</label>
                            <input type="radio" id="star2" name="stars" value="2" />
                            <label for="star2">2 stars</label>
                            <input type="radio" id="star1" name="stars" value="1" />
                            <label for="star1">1 star</label>
                        </fieldset>
                    </div>
                """
            ), css_class='d-flex flex-column grey-text'),
            Row(
                Column('title', css_class='col-md mb-0'), css_class='grey-text'
            ),
            Row(
                Column(Field('comment', type='textarea', rows="5"), css_class='mb-0 grey-text'),
            ),
            Submit('submit', _('Salva'), css_class='btn btn-info'),
            Submit('cancel', _('Annulla'), css_class='btn btn-danger', formnovalidate='formnovalidate'),
        )

    class Meta:
        model = Review
        fields = (
            'title',
            'stars',
            'comment'
        )
        labels = {
            'title': 'Titolo',
            'stars': 'Voto',
            'comment': 'Commento'
        }