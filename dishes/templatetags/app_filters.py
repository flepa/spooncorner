from statistics import mean

from django.template.defaulttags import register

from dishes.models import Dish, Review, OrderHeader


def return_dishes_from_category(query_set, category):
    """
    Given a queryset return only the dishes from a specific category,
    :param query_set: All the dishes.
    :param category: The category to extract.
    :return: Only the dishes of a specific category.
    """
    return query_set.filter(category=category)


@register.filter
def get_main_dishes(query_set):
    """
    Returns the main dishes from the query_set of all the dishes.
    :param query_set: All the dishes.
    :return: Only the dishes of the main dishes category.
    """
    return return_dishes_from_category(query_set, Dish.MAIN)


@register.filter
def get_burgers(query_set):
    """
    Returns the burgers from the query_set of all the dishes.
    :param query_set: All the dishes.
    :return: Only the dishes of the burgers category.
    """
    return return_dishes_from_category(query_set, Dish.BURGER)


@register.filter
def get_sides(query_set):
    """
    Returns the sides from the query_set of all the dishes.
    :param query_set: All the dishes.
    :return: Only the dishes of the sides category.
    """
    return return_dishes_from_category(query_set, Dish.SIDE)


@register.filter
def get_desserts(query_set):
    """
    Returns the desserts from the query_set of all the dishes.
    :param query_set: All the dishes.
    :return: Only the dishes of the desserts category.
    """
    return return_dishes_from_category(query_set, Dish.DESSERT)


@register.filter
def get_dish_from_list(dishes, index):
    """
    Returns a specific dish from the given dishes list.
    :param dishes: List of dishes.
    :param index: Current element to return.
    :return: The dish at the given index.
    """
    return dishes[index]


@register.filter
def get_preparation_time(order, user):
    """
    Returns the expected preparation time of a user's order. The time is calculated
    depending on the user zone.
    :param order: Current order from which to take the expected preparation time.
    :param user: Order owner.
    :return: The expected time
    """
    order_items = order.get_items
    if order.delivery_type:
        preparation_time = user.profile.get_expected_delivery_time
    else:
        preparation_time = 0
    for item in order_items:
        preparation_time += item.dish.cook_time
    return preparation_time


@register.filter
def get_in_progress(order_queryset):
    """
    Retrieves the orders in progress.
    :param order_queryset: Queryset of all the orders.
    :return: A queryset with only the orders in progress.
    """
    return order_queryset.exclude(state=OrderHeader.DONE)


@register.filter
def get_completed(order_queryset):
    """
    Retrieves the orders completed.
    :param order_queryset: Queryset of all the orders.
    :return: A queryset with only the orders completed.
    """
    return order_queryset.filter(state=OrderHeader.DONE)


@register.filter
def exclude_ordered_dishes(category_query_set, user):
    """
    Shows only the first three dishes that have never been ordered by the current
    user (three dishes for each category).
    :param category_query_set: All the dishes of a specific category.
    :param user: Current user to profile.
    :return: The first three dishes that have never been ordered.
    """
    dishes_to_display = 3
    dishes = [dish for dish in category_query_set if dish not in user.profile.get_all_ordered_dishes[dish.category]]
    return dishes[:dishes_to_display]