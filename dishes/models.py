from statistics import mean

from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from restaurant_project import settings


class Dish(models.Model):
    """
    Model that represent a dish, a dish is created by the manager account.
    """
    images_path = 'uploads/'
    MAIN = 'MD'
    BURGER = 'BU'
    SIDE = 'SI'
    DESSERT = 'DE'
    CATEGORY_CHOICES = [
        (MAIN, _('Piatto principale')),
        (BURGER, _('Burger')),
        (SIDE, _('Contorno')),
        (DESSERT, _('Dessert')),
    ]
    name = models.CharField(max_length=150, unique=True)
    category = models.CharField(max_length=2, choices=CATEGORY_CHOICES, default=MAIN)
    ingredients = models.TextField(max_length=255)
    cook_time = models.IntegerField()
    price = models.DecimalField(max_digits=5, decimal_places=2)
    image = models.ImageField(upload_to=images_path,
                              default=images_path + 'bread.png')

    @classmethod
    def get_dish_categories(cls):
        """
        Returns a dictionary with all the possible categories of a dish as keys and
        empty arrays as values.
        :return: a dictionary with a set of empty arrays for each category.
        """
        return {category: [] for category, description in cls.CATEGORY_CHOICES}

    @property
    def get_reviews(self):
        """
        Returns all the reviews associated to the current dish.
        :return: The review queryset.
        """
        return Review.objects.filter(dish=self)

    @property
    def get_avg_revs(self):
        """
        Returns the average score of the reviews related to the dish.
        :return: The average value rounded to one decimal position.
        """
        revs_list = Review.objects.filter(dish=self).values_list('stars', flat=True)
        return round(mean(revs_list), 1) if len(revs_list) > 0 else 0

    def __repr__(self):
        return self.name

    def __str__(self):
        return f'{self.name}'

    class Meta:
        ordering = ['-pk']
        verbose_name_plural = _('Dishes')


class Review(models.Model):
    """
    Model that represent a Review for a Dish, a review is written by a registered user.
    """
    title = models.CharField(max_length=150)
    stars = models.IntegerField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='user_reviews')
    dish = models.ForeignKey(Dish, on_delete=models.CASCADE, related_name='dish_reviews')
    comment = models.TextField(max_length=2000)
    date = models.DateTimeField(default=timezone.now)

    def __repr__(self):
        return f'[ {self.title} - {self.stars} ] di {self.user}'

    def __str__(self):
        return f'[ {self.title} - {self.stars} ] di {self.user}'

    class Meta:
        ordering = ['-date']


class OrderHeader(models.Model):
    """
    Model that represent an Order, every order is created as a CART when the
    user create his profile or when he complete an order from the "Cart" page.
    """
    CART = 'CA'
    ORDERED = 'OR'
    PROCESSED = 'PR'
    SHIPPED = 'SH'  # at home
    READY = 'RE'  # takeaway
    DONE = 'DO'
    ORDER_STATES = [
        (CART, _('Carrello')),
        (ORDERED, _('Ordinato')),
        (PROCESSED, _('In preparazione')),
        (READY, _('Pronto')),
        (SHIPPED, _('In consegna')),
        (DONE, _('Completato')),
    ]
    code = models.CharField(max_length=150, null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='user_orders')
    amount = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    state = models.CharField(max_length=2, choices=ORDER_STATES, default=CART)
    delivery_type = models.BooleanField(default=False)  # False --> takeaway, True --> at home
    date = models.DateTimeField(default=timezone.now)
    date_change_state = models.DateTimeField(auto_now_add=True)
    notes = models.TextField(max_length=255, null=True, blank=True)

    @property
    def get_items(self):
        """
        Gets all the items associated to the current order (self).
        :return: queryset of the items associated to the current order.
        """
        return OrderItem.objects.filter(order=self)

    @property
    def get_items_count(self):
        """
        Gets number of the pieces of the items associated to the current order.
        :return: number of the pieces (integer).
        """
        return sum(OrderItem.objects.filter(order=self).values_list('pieces', flat=True))

    @property
    def get_amount(self):
        """
        Gets the amount of the current order, based on the sum of the price of
        all the order items.
        :return: total amount (integer)
        """
        return sum(list(OrderItem.objects.filter(order=self).values_list('price', flat=True)))

    @property
    def get_lexical_state(self):
        """
        Retrieves the lexical version of the current order state.
        :return: The description of the order state.
        """
        return [description
                for state, description in OrderHeader.ORDER_STATES
                if state == self.state][0]

    def __str__(self):
        return f'Ordine in stato {self.state} di {self.user}'

    def __repr__(self):
        return f'Ordine in stato {self.state} di {self.user}'

    class Meta:
        ordering = ['-date_change_state']


class OrderItem(models.Model):
    """
    Model that represent an order item, an order item is created when a user
    add a new dish in the cart.
    """
    order = models.ForeignKey(OrderHeader, on_delete=models.CASCADE, related_name='order_items')
    dish = models.ForeignKey(Dish, on_delete=models.CASCADE, related_name='dish_orders')
    pieces = models.IntegerField()
    price = models.DecimalField(max_digits=5, decimal_places=2)
    notes = models.CharField(max_length=150, null=True, blank=True)

    def __str__(self):
        return f'Elemento {self.dish} dell\'ordine {self.order}'

    def __repr__(self):
        return f'Elemento {self.dish} dell\'ordine {self.order}'
