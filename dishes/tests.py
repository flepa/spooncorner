from django.test import TestCase, Client
from dishes.models import Dish, OrderHeader
from user_management.models import PlatformUser, Profile
from django.urls import reverse

from decimal import Decimal


def create_dish(name, ingredients, cook_time, price):
    """
    Create a test dish.
    :param name: name of the dish
    :param ingredients: ingredients of the dish
    :param cook_time: cook time of the dish
    :param price: price of the dish
    :return: the dish instance
    """
    return Dish.objects.create(name=name, ingredients=ingredients, cook_time=cook_time, price=price)


def create_user(username, email, password):
    """
    Create a test user.
    :param username: username of the user
    :param email: email of the user
    :param password: password of the user
    :return: user instance
    """
    test_user = PlatformUser.objects.create_user(username=username, email=email, password=password)
    return test_user


def create_profile(first_name, last_name, user):
    """
    Create a test user profile and his cart.
    :param first_name:
    :param last_name:
    :param user:
    :return:
    """
    Profile.objects.create(first_name=first_name, last_name=last_name, user=user)
    user.profile.create_cart()


def add_item_to_cart(client, dish_name, pieces, price, order_item_pk=None):
    """
    Add an item to the user cart.
    :param client: client object that represent the user.
    :param dish_name: dish added to the cart.
    :param pieces: number of pieces ordered.
    :param price: price of the added dish.
    :param order_item_pk: primary key of the order item when a cart update is
    requested.
    :return: response of the client post.
    """
    post_data = {
        'order_item[dish_name]': dish_name,
        'order_item[quantity]': pieces,
        'order_item[amount]': price * pieces,
    }
    if order_item_pk:
        post_data['order_item[dish_pk]'] = order_item_pk
    return client.post(reverse('dishes:add-dish-to-cart'), data=post_data)


def remove_item_from_cart(client, order_item_pk):
    """
    Remove the given order item from the cart.
    :param client: client object that represent the user.
    :param order_item_pk: primary key of the order item that have to be deleted.
    :return: response of the client post.
    """
    return client.post(reverse('dishes:remove-from-cart'), data={'item_pk': order_item_pk})


class CartViewTests(TestCase):
    """
    Test Class to check the accuracy of the Cart View.
    """
    def setUp(self):
        """
        Set up of a sample test environment with a dummy client that
        impersonates the user and two basic dishes.
        :return: None.
        """
        self.client = Client()
        self.user = create_user('john_snow', 'johnsnow@gmail.com', 'gameofthrones')
        create_profile('John', 'Snow', self.user)
        self.dish1 = create_dish('dish 1', 'ingredients', 1, Decimal(5))
        self.dish2 = create_dish('dish 2', 'ingredients', 1, Decimal(10))

    def test_access_at_cart_page(self):
        """
        A non authenticated user can't access the cart page.
        :return: None.
        """
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.status_code, 302)
        self.client.login(email='johnsnow@gmail.com', password='gameofthrones')
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.status_code, 200)

    def test_cart_with_no_items(self):
        """
        No items in the cart -> zero element in the queryset.
        :return: None.
        """
        self.client.login(email='johnsnow@gmail.com', password='gameofthrones')
        order = OrderHeader.objects.get(user=self.user)
        response = self.client.get(reverse('dishes:cart'))
        print(response.context['orderitem_list'].count())
        self.assertEqual(response.context['orderitem_list'].count(), 0)

    def test_cart_with_item_from_menu_page(self):
        """
        Adding an item to the cart -> one element in the queryset.
        :return: None.
        """
        self.client.login(email='johnsnow@gmail.com', password='gameofthrones')
        response = add_item_to_cart(self.client, self.dish1.name, 1, self.dish1.price)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.context['orderitem_list'].count(), 1)

    def test_cart_adding_new_item_from_menu_page(self):
        """
        Adding an other item to a cart that already contains one item -> two
        element in the queryset and the amount is the sum of the price of the
        two dishes.
        :return: None.
        """
        self.client.login(email='johnsnow@gmail.com', password='gameofthrones')
        response = add_item_to_cart(self.client, self.dish1.name, 1, self.dish1.price)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.context['orderitem_list'].count(), 1)
        self.assertEqual(response.context['amount'], Decimal(self.dish1.price))
        response = add_item_to_cart(self.client, self.dish2.name, 1, self.dish2.price)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.context['orderitem_list'].count(), 2)
        self.assertEqual(response.context['amount'], Decimal(self.dish1.price) + Decimal(self.dish2.price))

    def test_cart_removing_item(self):
        """
        Removing an item from a cart that contains two items -> one item in the
        query set and the amount corresponds to the remained dish price.
        :return: None.
        """
        self.client.login(email='johnsnow@gmail.com', password='gameofthrones')
        response = add_item_to_cart(self.client, self.dish1.name, 1, self.dish1.price)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.context['orderitem_list'].count(), 1)
        self.assertEqual(response.context['amount'], Decimal(self.dish1.price))
        response = add_item_to_cart(self.client, self.dish2.name, 1, self.dish2.price)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.context['orderitem_list'].count(), 2)
        self.assertEqual(response.context['amount'], Decimal(self.dish1.price) + Decimal(self.dish2.price))
        response = remove_item_from_cart(self.client, response.context['orderitem_list'][0].pk)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.context['orderitem_list'].count(), 1)
        self.assertEqual(response.context['amount'], Decimal(self.dish2.price))

    def test_cart_adding_already_existing_item_from_menu_page(self):
        """
        Adding an item (from the menu page) to a cart which already contains
        that item -> the item is only updated, so: one item in the query set
        and the amount corresponds to the dish price * its quantity in the cart.
        :return: None.
        """
        self.client.login(email='johnsnow@gmail.com', password='gameofthrones')
        response = add_item_to_cart(self.client, self.dish1.name, 1, self.dish1.price)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.context['orderitem_list'].count(), 1)
        self.assertEqual(response.context['amount'], Decimal(self.dish1.price))
        response = add_item_to_cart(self.client, self.dish1.name, 1, self.dish1.price)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.context['orderitem_list'].count(), 1)
        self.assertEqual(response.context['amount'], Decimal(self.dish1.price) + Decimal(self.dish1.price))

    def test_cart_update_order_item_from_cart_page(self):
        """
        The cart already has two pieces of a dish, updating the cart item (from
        the cart page) needs its primary key, then the item is correctly
        updated: one item instad of two -> one item in the query set and the
        amount corresponds to a single dish price.
        :return: None.
        """
        self.client.login(email='johnsnow@gmail.com', password='gameofthrones')
        response = add_item_to_cart(self.client, self.dish1.name, 2, self.dish1.price)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.context['orderitem_list'].count(), 1)
        self.assertEqual(response.context['amount'], Decimal(self.dish1.price) + Decimal(self.dish1.price))
        response = add_item_to_cart(self.client,
                                    self.dish1.name,
                                    1,
                                    self.dish1.price,
                                    response.context['orderitem_list'][0].pk)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('dishes:cart'))
        self.assertEqual(response.context['orderitem_list'].count(), 1)
        self.assertEqual(response.context['amount'], Decimal(self.dish1.price))