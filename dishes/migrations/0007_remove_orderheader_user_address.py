# Generated by Django 3.0.7 on 2020-08-16 10:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dishes', '0006_auto_20200816_1039'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orderheader',
            name='user_address',
        ),
    ]
