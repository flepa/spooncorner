from decimal import Decimal

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView, TemplateView
from django.utils.translation import gettext_lazy as _

from dishes.decorators import review_owner_only
from dishes.forms import DishForm, OrderHeaderForm, ReviewForm
from dishes.models import Dish, Review, OrderHeader, OrderItem

from user_management.decorators import manager_only
from user_management.models import PlatformUser

from notifications.signals import notify


################ DISH ################

class MenuView(ListView):
    """
    View to display the Menu page.
    """
    model = Dish
    template_name = 'dishes/menu.html'


@method_decorator(manager_only, name='dispatch')
class DishAddView(CreateView):
    """
    View to add a dish to the menu.
    """
    model = Dish
    form_class = DishForm
    template_name = 'dishes/dish_add.html'
    success_url = reverse_lazy('dishes:menu')

    def post(self, request, *args, **kwargs):
        """
        In order to manage the cancel button from the dish form. If 'cancel'
        is in the request.POST, the dish must not be updated.
        :return: HTTP response.
        """
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse_lazy('dishes:menu'))
        else:
            return super(DishAddView, self).post(request, *args, **kwargs)


class DishDetailView(DetailView):
    """
    View to display the detail page of a dish.
    """
    model = Dish
    template_name = 'dishes/dish_detail.html'

    def get_context_data(self, **kwargs):
        """
        In order to retrieve the reviews associated to the requested dish.
        """
        context = super(DishDetailView, self).get_context_data(**kwargs)
        context['reviews'] = self.object.get_reviews
        return context


@method_decorator(manager_only, name='dispatch')
class DishUpdateView(UpdateView):
    """
    View to update an existing dish.
    """
    model = Dish
    form_class = DishForm
    template_name = 'dishes/dish_update.html'

    def post(self, request, *args, **kwargs):
        """
        In order to manage the cancel button from the dish form. If 'cancel'
        is in the request.POST, the dish must not be updated.
        :return: HTTP response.
        """
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse_lazy('dishes:dish-detail', kwargs={'pk': self.kwargs['pk']}))
        else:
            return super(DishUpdateView, self).post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('dishes:dish-detail', kwargs={'pk': self.object.pk})


@method_decorator(manager_only, name='dispatch')
class DishDeleteView(DeleteView):
    """
    View to delete an existing dish.
    """
    model = Dish
    template_name = 'dishes/dish_delete.html'
    success_url = reverse_lazy('dishes:menu')


class DishSearchView(ListView):
    """
    View to display the dishes that correspond to the search criteria.
    """
    model = Dish
    template_name = 'dishes/search_results.html'

    def get_queryset(self):
        """
        Retrieving the queryset with the resulting dishes.
        :return: queryset with the resulting dishes.
        """
        return Dish.objects.filter(name__icontains=self.request.GET.get('search'))


################ ORDER ################


class CartListView(LoginRequiredMixin, ListView):
    """
    View to display the elements in the Cart page.
    """
    model = OrderItem
    template_name = 'dishes/cart.html'
    order = None

    def get_queryset(self):
        """
        Retrieving the items in the user cart.
        :return: the queryset containing the items.
        """
        # self.order = OrderHeader.objects.get(user=self.request.user, state=OrderHeader.CART)
        self.order = self.request.user.profile.get_cart
        return self.order.get_items

    def get_context_data(self, *, object_list=None, **kwargs):
        """
        Retrieving the dishes associated to the order items and the total amount.
        :return: The view context.
        """
        context = super(CartListView, self).get_context_data(**kwargs)
        context['dishes'] = [Dish.objects.get(pk=item.dish_id) for item in context['orderitem_list']]
        context['amount'] = self.order.get_amount
        context['orderheader'] = self.order
        return context


class OrderRecapView(LoginRequiredMixin, DetailView):
    """
    View to display the latest information about the order before completing the
    procedure.
    """
    model = OrderHeader
    template_name = 'dishes/order_recap.html'

    def post(self, request, *args, **kwargs):
        """
        The manager must be notified about the new order and the order changes
        its state.
        :return: HTTP response.
        """
        manager = PlatformUser.objects.get(is_manager=True)
        user_order = self.request.user.profile.get_cart
        email_subject = _(f'Nuovo ordine [{user_order.code}] da parte di {self.request.user}')
        message = _('Recati su SpoonCorner per gestire il nuovo ordine!')
        if request.POST.get('deliveryType'):  # not takeaway -> delivery
            user_order.delivery_type = True
        user_order.amount = user_order.get_amount
        # the order change state from CART to ORDERED
        user_order.state = OrderHeader.ORDERED
        user_order.date = user_order.date_change_state = timezone.now()
        user_order.save()
        # a new cart must be created
        request.user.profile.create_cart()
        notify.send(self.request.user, recipient=manager, verb=email_subject)
        manager.email_user(subject=email_subject, message=message)
        return HttpResponseRedirect(reverse_lazy('dishes:order-recap', kwargs={'pk': self.kwargs['pk']}))


class OrderCompleteView(LoginRequiredMixin, TemplateView):
    """
    View to display the order complete message.
    """
    template_name = 'dishes/order_complete.html'

    def post(self, request, *args, **kwargs):
        """
        The user must be notified about the taking charge of his order.
        :return: HTTP response.
        """
        manager = PlatformUser.objects.get(is_manager=True)
        user_order = OrderHeader.objects.get(pk=self.kwargs['pk'], user=self.request.user)
        email_subject = _(f'Il tuo ordine [{user_order.code}] è stato preso in carico')
        message = _('Preparati ad assaporare un\'esperienza unica!\n\n'
                    + 'A presto,\n'
                    + 'Il team di SpoonCorner')
        if request.POST.get('notes'):
            user_order.notes = request.POST['notes']
        # the order became processed
        user_order.state = OrderHeader.PROCESSED
        user_order.date_change_state = timezone.now()
        user_order.save()
        notify.send(manager, recipient=self.request.user, verb=email_subject)
        self.request.user.email_user(subject=email_subject, message=message)
        return HttpResponseRedirect(reverse_lazy('dishes:order-complete', kwargs={'pk': self.kwargs['pk']}))


class OrderDetailView(LoginRequiredMixin, UpdateView):
    """
    View to display the details of an order, if the user is manager, the order
    state can be updated, otherwise the form is disabled.
    """
    model = OrderHeader
    template_name = 'dishes/order_detail.html'
    form_class = OrderHeaderForm
    success_url = reverse_lazy('dishes:order-list')

    def get_object(self, queryset=None):
        """
        The manager can access every order, otherwise a normal user can access
        only his orders.
        :return: queryset containing the corrisponding order.
        """
        if not self.request.user.is_manager:
            return get_object_or_404(OrderHeader, pk=self.kwargs['pk'], user=self.request.user)
        else:
            return get_object_or_404(OrderHeader, pk=self.kwargs['pk'])

    def get_form_kwargs(self):
        """
        The user must be passed to the form to check if is manager or not.
        :return: the form keyword args.
        """
        user = self.request.user
        form_kwargs = super().get_form_kwargs()
        form_kwargs.update({
            'initial': {
                'user': user
            }
        })
        return form_kwargs

    def post(self, request, *args, **kwargs):
        """
        In order to manage the cancel button from the dish form. If 'cancel'
        is in the request.POST, the dish must not be updated.
        :return: HTTP response.
        """
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse_lazy('dishes:order-list'))
        else:
            return super(OrderDetailView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        """
        The user must be notified if the order state became SHIPPED and the order
        have to be delivered or if the order state became READY and the order have
        to be picked up by the user.
        :param form: the order form.
        :return: form valid super method.
        """
        # the date_change_state have to be updated in any case
        form.instance.date_change_state = timezone.now()
        if form.instance.delivery_type and form.instance.state == OrderHeader.SHIPPED:
            manager = PlatformUser.objects.get(is_manager=True)
            email_subject = _('Il tuo ordine è in consegna!')
            message = _('Resisti, manca davvero poco!\n\n'
                        + 'A presto,\n'
                        + 'Il team di SpoonCorner')
            notify.send(manager, recipient=form.instance.user, verb=email_subject)
            form.instance.user.email_user(subject=email_subject, message=message)
        elif not form.instance.delivery_type and form.instance.state == OrderHeader.READY:
            manager = PlatformUser.objects.get(is_manager=True)
            email_subject = _('Il tuo ordine è pronto per essere ritirato!')
            message = _('I tuoi piatti fumanti sono in attesa presso il nostro ristorante, corri a prenderli!\n\n'
                        + 'A presto,\n'
                        + 'Il team di SpoonCorner')
            notify.send(manager, recipient=form.instance.user, verb=email_subject)
            form.instance.user.email_user(subject=email_subject, message=message)
        return super(OrderDetailView, self).form_valid(form)

    def get_context_data(self, *, object_list=None, **kwargs):
        """
        The dishes ordered by the user have to be displayed with the information
        about price and quantity contained in the order items.
        :return: The view context.
        """
        context = super(OrderDetailView, self).get_context_data(**kwargs)
        order_items = OrderItem.objects.filter(order=context['orderheader'])
        context['dishes'] = [Dish.objects.get(pk=item.dish_id) for item in order_items]
        context['orderitems'] = order_items
        return context


class OrderListView(LoginRequiredMixin, ListView):
    """
    View to display all the user orders (the manager can view all the platform
    orders).
    """
    model = OrderHeader
    template_name = 'dishes/order_list.html'

    def get_queryset(self):
        """
        Retrieves all the platform orders (manager) or all the user orders.
        :return: Order header queryset.
        """
        if self.request.user.is_manager:
            return OrderHeader.objects.all().exclude(state=OrderHeader.CART)
        else:
            return OrderHeader.objects.filter(user=self.request.user).exclude(state=OrderHeader.CART)


################ REVIEW ################


class ReviewCreateView(LoginRequiredMixin, CreateView):
    """
    View to add a review related to a dish.
    """
    model = Review
    form_class = ReviewForm
    template_name = 'dishes/review_add.html'
    success_url = None

    def post(self, request, *args, **kwargs):
        """
        In order to manage the cancel button from the dish form. If 'cancel'
        is in the request.POST, the dish must not be updated.
        :return: HTTP response.
        """
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse_lazy('dishes:dish-detail', kwargs={'pk': self.kwargs['pk']}))
        else:
            return super(ReviewCreateView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        """
        Once the review is created, the foreign keys must be inserted.
        :param form: review form.
        :return: form valid super method.
        """
        form.instance.dish = Dish.objects.get(pk=self.kwargs['pk'])
        form.instance.user = self.request.user
        return super(ReviewCreateView, self).form_valid(form)

    def get_success_url(self):
        """
        Once the review is successfully created, the user is redirected to the
        dish detail page.
        :return: HTTP response.
        """
        return reverse_lazy('dishes:dish-detail', kwargs={'pk': self.kwargs['pk']})

    def get_context_data(self, **kwargs):
        """
        The dish instance is needed for a correct display of the information.
        :return: The view context.
        """
        context = super(ReviewCreateView, self).get_context_data(**kwargs)
        context['dish'] = Dish.objects.get(pk=self.kwargs['pk'])
        return context


class ReviewDetailView(DetailView):
    """
    View to display the detail of an existing review.
    """
    model = Review
    template_name = 'dishes/review_detail.html'


@method_decorator(review_owner_only, name='dispatch')
class ReviewUpdateView(LoginRequiredMixin, UpdateView):
    """
    View to update an existing review.
    """
    model = Review
    form_class = ReviewForm
    template_name = 'dishes/review_add.html'

    def form_valid(self, form):
        """
        Once a review is updated, its date must be updated too.
        :param form: review form.
        :return: form valid super method.
        """
        form.instance.date = timezone.now()
        return super(ReviewUpdateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        """
        In order to manage the cancel button from the dish form. If 'cancel'
        is in the request.POST, the dish must not be updated.
        :return: HTTP response.
        """
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse_lazy('dishes:review-detail', kwargs={'pk': self.kwargs['pk']}))
        else:
            return super(ReviewUpdateView, self).post(request, *args, **kwargs)

    def get_success_url(self):
        """
        Once the review is successfully updated, the user is redirected to the
        dish detail page.
        :return: HTTP response.
        """
        return reverse_lazy('dishes:review-detail', kwargs={'pk': self.object.pk})


@method_decorator(review_owner_only, name='dispatch')
class ReviewDeleteView(LoginRequiredMixin, DeleteView):
    """
    View to delete an existing review.
    """
    model = Review
    template_name = 'dishes/review_delete.html'
    success_url = reverse_lazy('dishes:menu')


################ AJAX REQUESTS ################


def search_dishes(request):
    """
    Function called by an ajax request, it returns the dishes that correspond to
    the search criteria given in the search bar.
    :return: JsonResponse of the results.
    """
    query = request.GET.get('query')
    if query != '':
        results_dishes = list(Dish.objects.filter(name__icontains=query).values('id', 'name'))
    else:
        results_dishes = []
    return JsonResponse({'results_dishes': results_dishes})


@login_required
@require_POST
@csrf_protect
def add_dish_to_cart(request):
    """
    Function called by an ajax request, it adds a dish to the current user cart.
    :return: JsonResponse of the results.
    """
    user_cart = request.user.profile.get_cart
    dish = Dish.objects.get(name=request.POST.get('order_item[dish_name]'))  # dish_name is unique

    if not request.POST.get('order_item[dish_pk]'):  # request from menu page
        if not OrderItem.objects.filter(order=user_cart, dish=dish).exists():  # new instance
            order_item = OrderItem(order=user_cart, dish=dish, pieces=request.POST.get('order_item[quantity]'),
                                   price=request.POST.get('order_item[amount]'))
        else:  # already added item, a cart update is requested
            order_item = OrderItem.objects.get(order=user_cart, dish=dish)
            order_item.pieces += int(request.POST.get('order_item[quantity]'))
            order_item.price += Decimal(request.POST.get('order_item[amount]'))
    else:  # request from cart page
        order_item = OrderItem.objects.get(order=user_cart, dish=dish)
        order_item.pieces = int(request.POST.get('order_item[quantity]'))
        order_item.price = Decimal(request.POST.get('order_item[amount]'))
    order_item.save()
    return JsonResponse({
        'dish_name': dish.name,
        'item_price': order_item.price,
        'item_pieces': order_item.pieces,
        'cart_amount': user_cart.get_amount
    })


@login_required
def get_cart_items(request):
    """
    Function called by an ajax request, it returns the number of items in the
    current cart.
    :return: JsonResponse containing the items number.
    """
    user_cart = OrderHeader.objects.get(user=request.user, state=OrderHeader.CART)
    return JsonResponse({'item_number': user_cart.get_items_count})


@login_required
@require_POST
def remove_from_cart(request):
    """
    Function called by an ajax request, it removes an item from the cart.
    :return: JsonResponse containing the operation result.
    """
    OrderItem.objects.get(pk=request.POST.get('item_pk')).delete()
    return JsonResponse({'item_removed': True})
