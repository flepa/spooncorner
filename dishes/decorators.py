from django.http import Http404

from dishes.models import Review


def review_owner_only(view_func):
    """
    Decorator that returns a function which protects the access to users' review.
    Only the review owner can update or delete a review.
    :param view_func: View to decorate.
    :return: Decorated function.
    """
    def check_owner(request, *args, **kwargs):
        if not request.user == Review.objects.get(pk=kwargs['pk']).user:
            raise Http404
        return view_func(request, *args, **kwargs)
    return check_owner
