from django.contrib import admin
from dishes.models import Dish, Review, OrderHeader, OrderItem

admin.site.register(Dish)
admin.site.register(Review)
admin.site.register(OrderHeader)
admin.site.register(OrderItem)
