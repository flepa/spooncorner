var SINGLE_PRICE = 0

function getPrice(dishPrice, itemPieces) {
    /*
    * Return the effective price of the dish related to its quantity.
    * */
    return itemPieces * dishPrice;
}

function populateModal(dishTitle, dishPrice, itemPieces) {
    /*
    * Write the requested dish inside the cart modal.
    * */
    $('.modal-title').text(dishTitle);
    $('.amount').text('Totale ' + dishPrice + ' €');
    $('#item-pieces').val(itemPieces);
    $('#cart-modal').modal('show');
}

$(function () {
    // initialize bootstrap touchspin
    $("#item-pieces").TouchSpin({
        buttondown_class: "btn btn-outline-info",
        buttonup_class: "btn btn-outline-info"
    });
    $('.bootstrap-touchspin').addClass('touchspin')

    // reset modal value on hiding
    $('#cart-modal').on('hidden.bs.modal', function () {
        $('#item-pieces').val(1);
        SINGLE_PRICE = 0;
    });

    // update the amount depending on the selected quantity
    $('#item-pieces').on('change', function () {
        let itemPieces = parseInt($('#item-pieces').val());
        let dishPrice = getPrice(SINGLE_PRICE, itemPieces);

        $('.amount').text('Totale ' + dishPrice + ' €');
    });

    // populate modal with the correct data
    $('.cart-link').on('click', function () {
        let dishTitle = $(this).closest('.dish-content').find('.dish-title').text();
        let dishPrice = parseFloat($(this).closest('.dish-content').find('.price').text());
        let itemPieces;

        if (document.title === 'Carrello')
            itemPieces = parseInt($(this).closest('.dish-content').find('.item-pieces').text());
        else
            itemPieces = 1;

        SINGLE_PRICE = dishPrice / itemPieces;
        dishPrice = getPrice(SINGLE_PRICE, itemPieces)
        populateModal(dishTitle, dishPrice, itemPieces)
    });
});