$(function () {
    // display the current active page
    let title = '#' + document.title;
    $(title).addClass('active');

    // language fix for the dish form:
    $('label[for=id_image]').attr('data-browse', 'Carica');
});