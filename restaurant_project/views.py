from django.views.generic import TemplateView


class HomePage(TemplateView):
    """
    View to display the home page.
    """
    template_name = 'home.html'
